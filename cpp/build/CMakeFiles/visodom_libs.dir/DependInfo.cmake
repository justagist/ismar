# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/saif/Desktop/ismar/cpp/src/STAM.cpp" "/home/saif/Desktop/ismar/cpp/build/CMakeFiles/visodom_libs.dir/src/STAM.cpp.o"
  "/home/saif/Desktop/ismar/cpp/src/VideoSource.cpp" "/home/saif/Desktop/ismar/cpp/build/CMakeFiles/visodom_libs.dir/src/VideoSource.cpp.o"
  "/home/saif/Desktop/ismar/cpp/src/types.cpp" "/home/saif/Desktop/ismar/cpp/build/CMakeFiles/visodom_libs.dir/src/types.cpp.o"
  "/home/saif/Desktop/ismar/cpp/src/utils.cpp" "/home/saif/Desktop/ismar/cpp/build/CMakeFiles/visodom_libs.dir/src/utils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv"
  "/usr/local/include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
