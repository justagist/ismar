import numpy as np
import cv2 as cv2
import sys
import csv
import yaml
from map_memory import Map_Memory
import roslib
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from cv_cam import image_from_ardrone
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

class vis_odom():

    def __init__(self):#,scene,baseline_threshold = (175.0,50.0,35.0)):
        # self.scene_no = scene
        self.camera_matrix, self.distortion_coeff = self.extract_intrinsics()
        # self.scene_prefix = "S0%d_INPUT/S0%dL03_VGA/S0%dL03_VGA_"%(self.scene_no,self.scene_no,self.scene_no)
        self.patch_prefix = "test_patches/patches/"
        # self.base_thresh = baseline_threshold[self.scene_no-1]
        self.patch_3d_file = "patch_spinny.csv"
        self.bridge = CvBridge()
        self.index_counter = 0
        self.Done_updating =True
        # self.starting_trouble_fix = False

        self.baseline_threshold = 2.0
        self.LK_opt_flow_tol = 30
        self.match_feature_prob_tol = 0.9
        self.knn_ratio = 0.7 # Lowe's paper recommends 0.7 or 0.75

        self.detect_alg = cv2.xfeatures2d.SIFT_create() #cv.ORB()
        self.matcher = cv2.BFMatcher()
        self.localised_status = False
        ## ------------- alternate matcher - FLANN ------------------
        # self.orb_flann_index_params = dict(algorithm = FLANN_INDEX_LSH,table_number = 6{#12},key_size = 12{#20}, multi_probe_level = 1) {#2}
        # self.sift_flann_index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        # self.flann_search_params = dict(checks=50)   # or pass empty dictionary
        # self.matcher = cv2.FlannBasedMatcher(<index_params>,<search_params>)
        ## -----------------------------------------------------------
        self.tot_repr_error = 0

    def test_projection_and_update(self):
        pt2d = self.mapmem.patch_2d_pts[len(self.mapmem.patch_2d_pts)-1]
        pt3d = self.mapmem.patch_3d_pts[len(self.mapmem.patch_3d_pts)-1]
        proj_mat = self.mapmem.proj_mats[len(self.mapmem.proj_mats)-1]
        del_idx = []
        for i in range(len(pt2d)):
            hom_wrld = np.array([[pt3d[i][0]],[pt3d[i][1]],[pt3d[i][2]],[1]])
            hom_prj = np.dot(proj_mat,hom_wrld)
            prj = (hom_prj/hom_prj[2])[:2,0]
            err = np.linalg.norm(abs(pt2d[i]-prj))
            if err > 50:
                del_idx.append(i)
        self.mapmem.update_patches(pt2d,pt3d,'remove_from_last',del_idx)

    def match_features(self, img_gray, path_prefix, visualize_patch_flag=False):
        ## cv2.namedWindow("Initial Patches# uses the matchTemplate function to find the most likely match of the feature in the main image. The minMaxLoc function is used to find the most likely location of the feature in the image. 
        ## Returns an array of the most likey location for each of the features given in the first image.
        ## if visualize_patch_flag is set True, the patches will be marked and displayed in the first image. (Used for testing the function when there was only one image, and not a series of sequential images.)
        patch_no = 0
        present = True
        unmatched_ids = []
        length = 0
        while present == True:
            # print 'matching features: %d'%patch_no
            patch = cv2.imread(path_prefix+"patch_%04d.png"%patch_no)
            if patch is None:
                if patch_no == 0:
                    sys.exit('ERROR! NO PATCHES FOUND!')
                else:
                    # print "Total of %d patches obtained"%(length)
                    present = False
            else:
                patch = cv2.cvtColor(patch, cv2.COLOR_BGR2GRAY)
                w, h = patch.shape[::-1]
                result = cv2.matchTemplate(img_gray,patch,cv2.TM_CCOEFF_NORMED)
                min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
                
                patch_centre = ((max_loc[0]+max_loc[0]+w)/2,(max_loc[1]+max_loc[1]+h)/2)
                if max_val > self.match_feature_prob_tol:
                    # print patch_centre
                    if length == 0:
                        patch_2d = np.array([patch_centre],np.float64)
                    else:
                        patch_2d = np.vstack((patch_2d,[patch_centre]))
                    if visualize_patch_flag == True or not self.localised_status:
                        rect_start = max_loc
                        rect_end = (rect_start[0] + w, rect_start[1] + h)
                        cv2.rectangle(img_gray,rect_start, rect_end, 255, 2)
                        cv2.circle(img_gray, patch_centre, 2, 255, 3) 
                    length +=1
                else:
                    unmatched_ids.append(patch_no)
            patch_no += 1
        
        if visualize_patch_flag == True or not self.localised_status:
            cv2.imshow("Initial Patches",img_gray)
            cv2.waitKey(1)
        if length == 0:
            patch_2d = None
        return patch_2d,unmatched_ids
                                    
    def extract_intrinsics(self):
        stream = file("camera_info/ardrone_bottom.yaml")
        calib_data = yaml.load(stream)
        cam_info_width = calib_data['image_width']
        cam_info_height = calib_data['image_height']
        cam_info_K = calib_data['camera_matrix']['data']
        cam_mat = np.asarray(cam_info_K).reshape(3,3)
        cam_info_D = calib_data['distortion_coefficients']['data']
        dist_mat = np.asarray(cam_info_D)
        cam_info_R = calib_data['rectification_matrix']['data']
        cam_info_P = calib_data['projection_matrix']['data']
        cam_info_distortion_model = calib_data['distortion_model']
        return cam_mat,dist_mat

    def test_results(self, pts_3d, pts_2d, P_mat):
        tot_error = 0
        for i in range(len(pts_2d)):
            pts_world_hom = np.array([[pts_3d[i][0]],[pts_3d[i][1]],[pts_3d[i][2]],[1]])
            pts_2d_projected_hom = np.dot(P_mat,pts_world_hom)
            pts_2d_projected = (pts_2d_projected_hom/pts_2d_projected_hom[2])[:2,0]
            tot_error+=np.linalg.norm(abs(pts_2d[i]-pts_2d_projected))
            # print np.sqrt(np.sum(pts_2d[i]-pts_2d_projected)**2)
        self.tot_repr_error+=tot_error/len(pts_2d)
        print 'Mean Reprojection Error: %f'%(tot_error/len(pts_2d)),tot_error,len(pts_2d)
        self.mean_repr_error = tot_error/len(pts_2d)

    def get_projection_matrix(self, objectPoints, imagePoints):
        # print objectPoints
        # print imagePoints
        _, rvec, tvec, inliers = cv2.solvePnPRansac(objectPoints, imagePoints, self.camera_matrix, self.distortion_coeff,reprojectionError=1)
        K = self.camera_matrix
        R = cv2.Rodrigues(rvec)[0]
        T = tvec
        trans_mat = np.concatenate((R,T),axis=1)
        proj_mat = np.dot(K,trans_mat)
        print proj_mat
        print R
        print T
        return proj_mat,trans_mat

    def get3Ddata(self,csv_file):
        with open(csv_file, 'rb') as f:
            reader = csv.reader(f)
            x = []
            for row in reader:
                if len(x) == 0:
                    x = np.array([float(row[0]),float(row[1]),float(row[2])],np.float64)
                else:
                    x = np.vstack((x,[float(row[0]),float(row[1]),float(row[2])]))
        # print len(x)
        return x
           
    def check_baseline_and_update(self,ind):
        if len(self.mapmem.cam_poses)>1 and len(self.mapmem.cam_poses) == ind+1:
            baseline_status = cv2.norm(self.mapmem.cam_poses[ind-1]-self.mapmem.cam_poses[ind]) >= self.baseline_threshold
            if baseline_status:
                new2d,new3d = self.find_new_patches(ind)
                self.mapmem.update_patches(new2d,new3d,'add_modify',ind)
                new_proj,new_pose = self.get_projection_matrix(self.mapmem.patch_3d_pts[ind],self.mapmem.patch_2d_pts[ind])
                self.mapmem.store_proj_mat(new_proj,'replace',ind)
                self.mapmem.store_cam_pose(new_pose,'replace',ind)
        elif len(self.mapmem.cam_poses)>1 or ind>1:
            print 'Error: len(self.mapmem.pose_mats) = %d, index = %d'%(len(self.mapmem.pose_mats),ind)

    def find_new_patches(self,idx):
        kp_curr,des_curr = self.detect_alg.detectAndCompute(self.mapmem.image_mats[idx],None)
        kp_prev,des_prev = self.detect_alg.detectAndCompute(self.mapmem.image_mats[idx-1],None)
        match_data_curr2prev = self.matcher.knnMatch(des_curr,des_prev, k=2)
        match_data_prev2curr = self.matcher.knnMatch(des_prev,des_curr, k=2)
        good_match_data_curr = self.ratio_test(match_data_curr2prev)
        good_match_data_prev = self.ratio_test(match_data_prev2curr)
        mutual_match_pts_curr,mutual_match_pts_prev = self.find_mutual_matches(good_match_data_curr,good_match_data_prev,kp_curr,kp_prev)
        new_2D_curr,new_2D_prev = self.find_Fmat_n_optimise_matches(mutual_match_pts_curr,mutual_match_pts_prev)
        und_2D_curr,und_2D_prev = self.undistort_patches(new_2D_curr.reshape(-1,1,2),new_2D_prev.reshape(-1,1,2))
        new_3D_curr = self.triangulate_pts(und_2D_curr,und_2D_prev,idx)
        return und_2D_curr[:,0,:],new_3D_curr
    
    def ratio_test(self,matches):# ratio test as per Lowe's paper (for FLANN)
        good = []
        i = 0
        for m,n in matches:
            if m.distance < self.knn_ratio*n.distance:
                good.append(matches[i])
            i += 1
        return good    

    def find_mutual_matches(self,mtch1,mtch2,kp1,kp2):
        mutual_match = []
        k = 0
        for i in range(len(mtch1)):
            for j in range(len(mtch2)):
                if mtch1[i][0].queryIdx == mtch2[j][0].trainIdx and mtch2[j][0].queryIdx == mtch1[i][0].trainIdx:
                    x2 = kp1[mtch1[i][0].queryIdx].pt[0]
                    y2 = kp1[mtch1[i][0].queryIdx].pt[1]
                    x1 = kp2[mtch1[i][0].trainIdx].pt[0]
                    y1 = kp2[mtch1[i][0].trainIdx].pt[1]
                    if k == 0:
                        pts1 = np.array([[x1,y1]],np.float64)
                        pts2 = np.array([[x2,y2]],np.float64)
                        k=1
                    else:
                        pts1 = np.vstack((pts1,[[x1,y1]]))
                        pts2 = np.vstack((pts2,[[x2,y2]]))
        return pts1,pts2

    def find_Fmat_n_optimise_matches(self,pts1,pts2):
        F_mat, inliers = cv2.findFundamentalMat(np.float32(pts2),np.float32(pts1),cv2.FM_RANSAC,param1 = 1.0,param2=0.99)#dist,confidence
        j = 0
        # print 'reaching here'
        for ins in range(len(inliers)):
            if inliers[ins] == 1:
                if j == 0:
                    # print match[ins]
                    # print pts1[ins]
                    # print pts2[ins]
                    new2d_1 = np.array([pts1[ins]],np.float64)
                    new2d_2 = np.array([pts2[ins]],np.float64)
                    j = 1
                else:
                    new2d_1 = np.vstack((new2d_1,[pts1[ins]]))
                    new2d_2 = np.vstack((new2d_2,[pts2[ins]]))
        return new2d_1,new2d_2

    def undistort_patches(self,p1,p2):
        und1 = cv2.undistortPoints(p1, self.camera_matrix, self.distortion_coeff,P=self.camera_matrix)
        und2 = cv2.undistortPoints(p2, self.camera_matrix, self.distortion_coeff,P=self.camera_matrix)
        return und1,und2

    def triangulate_pts(self,p_curr,p_prev,indx):
        # self.mapmem.proj_mats[indx]
        hom_curr_4D = cv2.triangulatePoints(self.mapmem.proj_mats[indx-1], self.mapmem.proj_mats[indx], p_prev, p_curr)
        curr_3D = hom_curr_4D/hom_curr_4D[3] #recovering 3d pots from homogeneneous
        curr_3D =  curr_3D.T
        return curr_3D[:,:3]
        # print (hom_curr_4D[0][0],hom_curr_4D[0][1],hom_curr_4D[0][2],hom_curr_4D[0][3]),len(hom_curr_4D)

    def updatepatch_lkt(self,prev_img,curr_img,prev_2d):
        patch, patch_status, err = cv2.calcOpticalFlowPyrLK(prev_img,curr_img,np.float32(prev_2d),np.float32(prev_2d).copy())
        j = 0
        lost_point_indices = []
        for i in range(len(patch)):
            if patch_status[i] == 1 and err[i] < self.LK_opt_flow_tol:
                if j == 0:
                    new_patch = np.array([patch[i]],np.float64)
                    j = 1
                else:
                    new_patch = np.vstack((new_patch,[patch[i]]))
            else:
                lost_point_indices.append(i)
        return new_patch,lost_point_indices

    def process(self,image,index):
        imgs = image
        gray_img = cv2.cvtColor(imgs, cv2.COLOR_BGR2GRAY)
        
        # self.localised_status = True
        if index == 0:# or self.starting_trouble_fix:
            try:
                open("patch_spinny.csv", 'rb')
                self.mapmem.clear_memory()
                temp_patch_3D = self.get3Ddata(self.patch_3d_file)
                patch_2D, idx2del = self.match_features(gray_img,self.patch_prefix)
                if len(patch_2D) < 12:
                    raise ValueError("Very few patches matched. Matched features = %d"%len(patch_2D))
                else: 
                    self.localised_status = True
                    cv2.destroyAllWindows()
                # projection_matrix,cam_pose = self.get_projection_matrix(patch_3D, patch_2D, self.camera_matrix, self.distortion_coeff)
            except IOError:
                self.localised_status = False
                print 'No file of Patches located.'
            except ValueError as e:
                temp_patch_3D = None
                patch_2D = None
                projection_matrix = None
                cam_pose = None
                idx2del = None
                self.localised_status = False
                print e
        else:
            # print 'ok'
            patch_2D,idx2del = self.updatepatch_lkt(self.mapmem.image_mats[index-1],gray_img,self.mapmem.patch_2d_pts[index-1])
            temp_patch_3D = self.mapmem.patch_3d_pts[index-1]
        if self.localised_status:
            self.mapmem.store_image_mats(gray_img)
            self.mapmem.update_patches(patch_2D,temp_patch_3D,'remove_lost',idx2del)
            patch_3D = self.mapmem.patch_3d_pts[index]
            projection_matrix,cam_pose = self.get_projection_matrix(patch_3D, patch_2D)
            print len(patch_3D),len(patch_2D)
            self.mapmem.store_proj_mat(projection_matrix)
            self.mapmem.store_cam_pose(cam_pose)
            # # print patch_2D
        return imgs
        

    def visualize_cam_pose(self,pose_mat):
        # R = pose_mat[:,0:3]
        # T = pose_mat[:,3]
        # pose = np.dot(-(np.linalg.inv(R)),T)
        # print pose
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        # theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
        poses = np.asarray(self.mapmem.cam_poses).reshape(-1,3)
        y = poses[:,1]/1000
        x = poses[:,0]/1000
        z = poses[:,2]/1000
        ax.scatter(x, y, z, c='r', marker='o')
        ax.legend()
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        # ax.invert_zaxis()
        ax.invert_yaxis()
        ax.set_xlim(-0, 3)
        ax.set_ylim(-1, 2)
        ax.set_zlim( 0, 2)

    def start(self):
        self.rate = rospy.Rate(1000)
        self.mapmem = Map_Memory()
        rospy.Subscriber("/ardrone/bottom/image_raw",Image,self.callback)
        # rospy.Subscriber("/ardrone/bottom/image_raw",Image,self.callback)
        try:
            rospy.spin()
        except KeyboardInterrupt:
            print("Shutting down")
            cv2.destroyAllWindows()
        print 'total mean error:',self.tot_repr_error/self.index_counter
        self.visualize_cam_pose(self.mapmem.cam_poses[len(self.mapmem.cam_poses)-1])
        plt.show()

    def callback(self,data):
        if self.Done_updating:
            self.Done_updating = False
            try:
                cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            except CvBridgeError as e:
                print(e)
            (rows,cols,channels) = cv_image.shape
            # cv2.imshow("window",cv_image)
            # cv2.waitKey(1)
            print '\nframe counter:',self.index_counter,
            curr_img = self.process(cv_image,self.index_counter)
            if self.localised_status:
                if len(self.mapmem.patch_2d_pts[self.index_counter])<50 and self.index_counter>1:
                    self.check_baseline_and_update(self.index_counter)
                # if i%5 == 0:
                self.test_projection_and_update()
                # self.mapmem.store_patches(pts_2d,pts_3d,self.index_counter)#########?????????????
                # print len(self.mapmem.patch_2d_pts[self.index_counter]),len(self.mapmem.patch_3d_pts[self.index_counter])#,len(self.mapmem.proj_mats),len(self.mapmem.image_mats),len(self.mapmem.stored_mats),len(self.mapmem.pose_mats),len(self.mapmem.cam_poses)
                for point in self.mapmem.patch_2d_pts[self.index_counter]:
                    cv2.circle(curr_img, (int(point[0]),int(point[1])), 1, (0,0,255), 3)
                cv2.imshow("Tracked Points",curr_img)
                cv2.waitKey(1)
                self.test_results(self.mapmem.patch_3d_pts[self.index_counter],self.mapmem.patch_2d_pts[self.index_counter],self.mapmem.proj_mats[self.index_counter])
                print self.mapmem.cam_poses[len(self.mapmem.cam_poses)-1]
                self.index_counter +=1
            self.Done_updating = True
            self.rate.sleep()

