// Auto-generated. Do not edit!

// (in-package visual_odometry_stam.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class TransmatMsg {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.vals = null;
    }
    else {
      if (initObj.hasOwnProperty('vals')) {
        this.vals = initObj.vals
      }
      else {
        this.vals = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type TransmatMsg
    // Serialize message field [vals]
    bufferOffset = _arraySerializer.float64(obj.vals, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type TransmatMsg
    let len;
    let data = new TransmatMsg(null);
    // Deserialize message field [vals]
    data.vals = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.vals.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'visual_odometry_stam/TransmatMsg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '89b4801008d1910f88994e9164bb7ddd';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[] vals
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new TransmatMsg(null);
    if (msg.vals !== undefined) {
      resolved.vals = msg.vals;
    }
    else {
      resolved.vals = []
    }

    return resolved;
    }
};

module.exports = TransmatMsg;
