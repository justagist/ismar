; Auto-generated. Do not edit!


(cl:in-package visual_odometry_stam-msg)


;//! \htmlinclude TransmatMsg.msg.html

(cl:defclass <TransmatMsg> (roslisp-msg-protocol:ros-message)
  ((vals
    :reader vals
    :initarg :vals
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass TransmatMsg (<TransmatMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TransmatMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TransmatMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name visual_odometry_stam-msg:<TransmatMsg> is deprecated: use visual_odometry_stam-msg:TransmatMsg instead.")))

(cl:ensure-generic-function 'vals-val :lambda-list '(m))
(cl:defmethod vals-val ((m <TransmatMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader visual_odometry_stam-msg:vals-val is deprecated.  Use visual_odometry_stam-msg:vals instead.")
  (vals m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TransmatMsg>) ostream)
  "Serializes a message object of type '<TransmatMsg>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'vals))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'vals))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TransmatMsg>) istream)
  "Deserializes a message object of type '<TransmatMsg>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'vals) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'vals)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TransmatMsg>)))
  "Returns string type for a message object of type '<TransmatMsg>"
  "visual_odometry_stam/TransmatMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TransmatMsg)))
  "Returns string type for a message object of type 'TransmatMsg"
  "visual_odometry_stam/TransmatMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TransmatMsg>)))
  "Returns md5sum for a message object of type '<TransmatMsg>"
  "89b4801008d1910f88994e9164bb7ddd")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TransmatMsg)))
  "Returns md5sum for a message object of type 'TransmatMsg"
  "89b4801008d1910f88994e9164bb7ddd")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TransmatMsg>)))
  "Returns full string definition for message of type '<TransmatMsg>"
  (cl:format cl:nil "float64[] vals~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TransmatMsg)))
  "Returns full string definition for message of type 'TransmatMsg"
  (cl:format cl:nil "float64[] vals~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TransmatMsg>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'vals) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TransmatMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'TransmatMsg
    (cl:cons ':vals (vals msg))
))
