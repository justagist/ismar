
(cl:in-package :asdf)

(defsystem "visual_odometry_stam-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "TransmatMsg" :depends-on ("_package_TransmatMsg"))
    (:file "_package_TransmatMsg" :depends-on ("_package"))
  ))