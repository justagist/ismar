;; Auto-generated. Do not edit!


(when (boundp 'visual_odometry_stam::TransmatMsg)
  (if (not (find-package "VISUAL_ODOMETRY_STAM"))
    (make-package "VISUAL_ODOMETRY_STAM"))
  (shadow 'TransmatMsg (find-package "VISUAL_ODOMETRY_STAM")))
(unless (find-package "VISUAL_ODOMETRY_STAM::TRANSMATMSG")
  (make-package "VISUAL_ODOMETRY_STAM::TRANSMATMSG"))

(in-package "ROS")
;;//! \htmlinclude TransmatMsg.msg.html


(defclass visual_odometry_stam::TransmatMsg
  :super ros::object
  :slots (_vals ))

(defmethod visual_odometry_stam::TransmatMsg
  (:init
   (&key
    ((:vals __vals) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _vals __vals)
   self)
  (:vals
   (&optional __vals)
   (if __vals (setq _vals __vals)) _vals)
  (:serialization-length
   ()
   (+
    ;; float64[] _vals
    (* 8    (length _vals)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[] _vals
     (write-long (length _vals) s)
     (dotimes (i (length _vals))
       (sys::poke (elt _vals i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[] _vals
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _vals (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _vals i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(setf (get visual_odometry_stam::TransmatMsg :md5sum-) "89b4801008d1910f88994e9164bb7ddd")
(setf (get visual_odometry_stam::TransmatMsg :datatype-) "visual_odometry_stam/TransmatMsg")
(setf (get visual_odometry_stam::TransmatMsg :definition-)
      "float64[] vals

")



(provide :visual_odometry_stam/TransmatMsg "89b4801008d1910f88994e9164bb7ddd")


