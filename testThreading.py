import threading
import time
import sys


def worker():
    global s
    s = 0
    while True:
        s+=1

def my_service():
    global s
    # print threading.currentThread().getName(), 'Starting'
    # time.sleep(3)
    while True:
        if s == 4:
            print 'got it'
            sys.exit()
        else:
            print '\\\b\n','|\b\n','/\b\n','--\b\n'
    # print threading.currentThread().getName(), 'Exiting'

t = threading.Thread(name='my_service', target=my_service)
w = threading.Thread(name='worker', target=worker)
# w2 = threading.Thread(target=worker) # use default name

w.start()
# w2.start()
t.start()